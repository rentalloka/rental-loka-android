package id.rentalloka.application.model

data class PropertyCategory(

    val id: Int? = null,

    val title: String? = null

)
