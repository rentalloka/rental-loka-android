package id.rentalloka.application.model

data class PropertyList(

    val product_id: Int? = null,

    val thumbnail: String? = null,

    val location: String? = null,

    val price: String? = null,

    val real_price: Int? = null,

    val images: List<String> = listOf()

)
