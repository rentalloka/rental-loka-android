package id.rentalloka.application.model

data class RegisterResponse(

    val access_token: String

)
