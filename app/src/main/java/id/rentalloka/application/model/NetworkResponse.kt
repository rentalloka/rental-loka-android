package id.rentalloka.application.model

sealed class NetworkResponse<T> {
    data class Success<T>(val response: T): NetworkResponse<T>()
    data class Error<T>(val error: String?): NetworkResponse<T>()
    data class Load<T>(val loading: Boolean): NetworkResponse<T>()
}