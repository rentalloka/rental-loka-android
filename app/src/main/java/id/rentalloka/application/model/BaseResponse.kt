package id.rentalloka.application.model

data class BaseResponse<M, E>(

    val code: Int? = null,

    val status: String? = null,

    val data: M? = null,

    val errors: E? = null

)
