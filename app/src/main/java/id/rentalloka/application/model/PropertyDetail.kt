package id.rentalloka.application.model

data class PropertyDetail(

    val product_id: Int? = null,

    val image_url: List<String> = listOf(),

    val title: String? = null,

    val rating: Int? = null,

    val location: String? = null,

    val longitude: String? = null,

    val latitude: String? = null,

    val overview: String? = null,

    val price: String? = null,

    val real_price: Int? = null,

    val owner: Profile? = null,

    val features: BasicFeatures? = null,

)
