package id.rentalloka.application.model

data class LoginResponse(

    val access_token: String

)
