package id.rentalloka.application.model

data class AuthenticationParams(

    val email: String? = null,

    val password: String? = null,

    val phone: String? = null,

    val name: String? = null,

    val token: String? = null

)
