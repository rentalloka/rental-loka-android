package id.rentalloka.application.model

data class BasicFeatures(

    val wide: String? = null,

    val bathroom: Int? = null,

    val bedroom: Int? = null

)
