package id.rentalloka.application.model

data class Profile(

    val id: Int? = null,

    val name: String? = null,

    val is_verified: Boolean? = null,
    
    val description: String? = null

)
