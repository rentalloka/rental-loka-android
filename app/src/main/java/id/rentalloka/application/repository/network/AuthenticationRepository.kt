package id.rentalloka.application.repository.network

import id.rentalloka.application.model.BaseResponse
import id.rentalloka.application.model.Error
import id.rentalloka.application.model.LoginResponse
import id.rentalloka.application.model.RegisterResponse
import io.reactivex.rxjava3.core.Single

interface AuthenticationRepository {

    fun login(email: String, password: String): Single<BaseResponse<LoginResponse, Error>>

    fun register(name: String, email: String, phone: String, password: String): Single<BaseResponse<RegisterResponse, Error>>

    fun logout(token: String): Single<BaseResponse<Nothing, Error>>

}