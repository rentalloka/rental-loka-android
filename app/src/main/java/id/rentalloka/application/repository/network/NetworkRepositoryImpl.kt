package id.rentalloka.application.repository.network

import id.rentalloka.application.model.BaseResponse
import id.rentalloka.application.model.Error
import id.rentalloka.application.model.LoginResponse
import id.rentalloka.application.model.RegisterResponse
import id.rentalloka.application.service.NetworkService
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class NetworkRepositoryImpl @Inject constructor(
    private val networkService: NetworkService
): AuthenticationRepository {

    override fun login(
        email: String,
        password: String
    ): Single<BaseResponse<LoginResponse, Error>> {
        return networkService.login(email, password)
    }

    override fun register(
        name: String,
        email: String,
        phone: String,
        password: String
    ): Single<BaseResponse<RegisterResponse, Error>> {
        return networkService.register(name, email, phone, password)
    }

    override fun logout(token: String): Single<BaseResponse<Nothing, Error>> {
        return networkService.logout(token)
    }


}