package id.rentalloka.application.service

import id.rentalloka.application.model.BaseResponse
import id.rentalloka.application.model.Error
import id.rentalloka.application.model.LoginResponse
import id.rentalloka.application.model.RegisterResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Header
import retrofit2.http.POST

interface NetworkService {

    @FormUrlEncoded
    @POST("/auth/login")
    fun login(
        @Field("email") email: String,
        @Field("password") password: String
    ): Single<BaseResponse<LoginResponse, Error>>

    @FormUrlEncoded
    @POST("/auth/register")
    fun register(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("phone") phone: String,
        @Field("password") password: String
    ): Single<BaseResponse<RegisterResponse, Error>>

    @FormUrlEncoded
    @POST("/auth/logout")
    fun logout(
        @Header("Authorization") token: String
    ): Single<BaseResponse<Nothing, Error>>

}