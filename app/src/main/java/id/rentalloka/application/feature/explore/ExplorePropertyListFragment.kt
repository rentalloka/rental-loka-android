package id.rentalloka.application.feature.explore

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import id.rentalloka.application.R
import id.rentalloka.application.base.BaseFragment
import id.rentalloka.application.base.BaseRecyclerViewAdapter
import id.rentalloka.application.databinding.FragmentExplorePropertyListBinding
import id.rentalloka.application.databinding.ItemExplorePropertyBinding
import id.rentalloka.application.feature.explore.adapter.PropertyImagesAdapter
import id.rentalloka.application.helper.DummyData
import id.rentalloka.application.helper.DummyData.Companion.getPropertyList
import id.rentalloka.application.helper.PropertyType
import id.rentalloka.application.helper.PropertyType.PROPERTY_TYPE_NAME
import id.rentalloka.application.model.PropertyList

@AndroidEntryPoint
class ExplorePropertyListFragment : BaseFragment<FragmentExplorePropertyListBinding>() {

    private lateinit var propertyContentAdapter: BaseRecyclerViewAdapter<ItemExplorePropertyBinding, PropertyList>

    override fun setupViewBinding(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentExplorePropertyListBinding {
        return FragmentExplorePropertyListBinding::inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var currentPosition = 0
        arguments?.takeIf { it.containsKey(CATEGORY_POSITION) }?.apply {
            currentPosition = getInt(CATEGORY_POSITION)
        }
        val propertyList = getPropertyList(currentPosition + 1)

        propertyContentAdapter = BaseRecyclerViewAdapter(propertyList, ItemExplorePropertyBinding::inflate) { item, itemBinding ->
            itemBinding.itemExplorePropertyImages.apply {
                adapter = PropertyImagesAdapter(requireParentFragment(), item.images)
            }
            itemBinding.itemExplorePropertyImagesDot.setViewPager2(itemBinding.itemExplorePropertyImages)
            itemBinding.itemExplorePropertyTitle.text = item.location
            itemBinding.itemExplorePropertyPrice.text = item.price
        }

        binding.explorePropertyListRv.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = propertyContentAdapter
        }
    }

    companion object {
        const val CATEGORY_POSITION = "category_position"
    }

}