package id.rentalloka.application.feature.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import id.rentalloka.application.base.BaseRecyclerViewAdapter
import id.rentalloka.application.base.BaseFragment
import id.rentalloka.application.config.Action
import id.rentalloka.application.config.ProfileFragmentConfig
import id.rentalloka.application.databinding.FragmentProfileBinding
import id.rentalloka.application.databinding.ItemProfileActionBinding

@AndroidEntryPoint
class ProfileFragment : BaseFragment<FragmentProfileBinding>() {

    private lateinit var profileActionAdapter: BaseRecyclerViewAdapter<ItemProfileActionBinding, Action>

    override fun setupViewBinding(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentProfileBinding {
        return FragmentProfileBinding::inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // action list
        val actions = ProfileFragmentConfig.getConfigurations(requireContext())

        // set up adapter
        profileActionAdapter = BaseRecyclerViewAdapter(actions, ItemProfileActionBinding::inflate) { action, itemProfileActionBinding ->
            Glide.with(requireContext()).load(action.icon).into(itemProfileActionBinding.itemProfileActionIcon)
            itemProfileActionBinding.itemProfileActionTitle.text = action.title
            itemProfileActionBinding.itemProfileActionSubtitle.text = action.subtitle
            itemProfileActionBinding.itemProfileActionContainer.setOnClickListener {

                // change this code if intent action available
                if (action.actionTo != null) {
                    startActivity(action.actionTo)
                } else {
                    Toast.makeText(requireContext(), action.title, Toast.LENGTH_SHORT).show()
                }
            }
        }

        binding.profileSettingsRv.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = profileActionAdapter
        }
    }

}