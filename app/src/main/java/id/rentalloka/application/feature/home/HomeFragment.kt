package id.rentalloka.application.feature.home

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import com.bumptech.glide.Glide
import id.rentalloka.application.R
import id.rentalloka.application.base.BaseFragment
import id.rentalloka.application.base.BaseRecyclerViewAdapter
import id.rentalloka.application.databinding.FragmentHomeBinding
import id.rentalloka.application.databinding.ItemPropertyContentBinding
import id.rentalloka.application.feature.detail.PropertyDetailActivity
import id.rentalloka.application.feature.notifikasi.NotifikasiActivity
import id.rentalloka.application.feature.search.SearchActivity
import id.rentalloka.application.helper.DummyData.Companion.getPropertyList
import id.rentalloka.application.helper.PropertyType.APARTMENT
import id.rentalloka.application.helper.PropertyType.HOME_RENT
import id.rentalloka.application.helper.PropertyType.HOUSE
import id.rentalloka.application.helper.PropertyType.STALL
import id.rentalloka.application.model.PropertyList

class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    private lateinit var stallAdapter: BaseRecyclerViewAdapter<ItemPropertyContentBinding, PropertyList>
    private lateinit var houseRentAdapter: BaseRecyclerViewAdapter<ItemPropertyContentBinding, PropertyList>
    private lateinit var houseAdapter: BaseRecyclerViewAdapter<ItemPropertyContentBinding, PropertyList>
    private lateinit var apartmentAdapter: BaseRecyclerViewAdapter<ItemPropertyContentBinding, PropertyList>

    override fun setupViewBinding(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentHomeBinding {
        return FragmentHomeBinding::inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.homeNotificationBtn.setOnClickListener {
            startActivity(Intent(requireContext(), NotifikasiActivity::class.java))
        }

        binding.homeSearch.setOnClickListener {
            startActivity(Intent(requireContext(), SearchActivity::class.java))
        }

        binding.homeRandomSearchBtn.setOnClickListener {
            view?.findNavController().navigate(R.id.action_homeFragment_to_exploreFragment)
        }

        val stallList = getPropertyList(STALL)
        val houseRentList = getPropertyList(HOME_RENT)
        val houseList = getPropertyList(HOUSE)
        val apartmentList = getPropertyList(APARTMENT)

        stallAdapter = BaseRecyclerViewAdapter(
            stallList,
            ItemPropertyContentBinding::inflate
        ) { item, itemBinding ->
            Glide.with(requireContext()).load(item.thumbnail)
                .into(itemBinding.itemPropertyContentThumbnail)
            itemBinding.itemPropertyContentThumbnail.clipToOutline = true
            itemBinding.itemPropertyContentTitle.text = item.location
            itemBinding.itemPropertyContentPrice.text = item.price
            itemBinding.itemPropertyContentContainer.setOnClickListener {
                startActivity(Intent(requireContext(), PropertyDetailActivity::class.java))
            }
        }

        houseRentAdapter = BaseRecyclerViewAdapter(
            houseRentList,
            ItemPropertyContentBinding::inflate
        ) { item, itemBinding ->
            Glide.with(requireContext()).load(item.thumbnail)
                .into(itemBinding.itemPropertyContentThumbnail)
            itemBinding.itemPropertyContentThumbnail.clipToOutline = true
            itemBinding.itemPropertyContentTitle.text = item.location
            itemBinding.itemPropertyContentPrice.text = item.price
            itemBinding.itemPropertyContentContainer.setOnClickListener {
                startActivity(Intent(requireContext(), PropertyDetailActivity::class.java))
            }
        }

        houseAdapter = BaseRecyclerViewAdapter(
            houseList,
            ItemPropertyContentBinding::inflate
        ) { item, itemBinding ->
            Glide.with(requireContext()).load(item.thumbnail)
                .into(itemBinding.itemPropertyContentThumbnail)
            itemBinding.itemPropertyContentThumbnail.clipToOutline = true
            itemBinding.itemPropertyContentTitle.text = item.location
            itemBinding.itemPropertyContentPrice.text = item.price
            itemBinding.itemPropertyContentContainer.setOnClickListener {
                startActivity(Intent(requireContext(), PropertyDetailActivity::class.java))
            }
        }

        apartmentAdapter = BaseRecyclerViewAdapter(
            apartmentList,
            ItemPropertyContentBinding::inflate
        ) { item, itemBinding ->
            Glide.with(requireContext()).load(item.thumbnail)
                .into(itemBinding.itemPropertyContentThumbnail)
            itemBinding.itemPropertyContentThumbnail.clipToOutline = true
            itemBinding.itemPropertyContentTitle.text = item.location
            itemBinding.itemPropertyContentPrice.text = item.price
            itemBinding.itemPropertyContentContainer.setOnClickListener {
                startActivity(Intent(requireContext(), PropertyDetailActivity::class.java))
            }
        }

        binding.mainRvStall.apply {
            layoutManager = LinearLayoutManager(requireContext(), HORIZONTAL, false)
            adapter = stallAdapter
        }

        binding.mainRvHouseRent.apply {
            layoutManager = LinearLayoutManager(requireContext(), HORIZONTAL, false)
            adapter = houseRentAdapter
        }

        binding.mainRvHouse.apply {
            layoutManager = LinearLayoutManager(requireContext(), HORIZONTAL, false)
            adapter = houseAdapter
        }

        binding.mainRvApartment.apply {
            layoutManager = LinearLayoutManager(requireContext(), HORIZONTAL, false)
            adapter = apartmentAdapter
        }
    }

}