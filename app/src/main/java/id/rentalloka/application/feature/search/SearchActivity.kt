package id.rentalloka.application.feature.search

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.rentalloka.application.R

class SearchActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
    }
}