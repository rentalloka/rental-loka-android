package id.rentalloka.application.feature

import android.os.Bundle
import android.view.LayoutInflater
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import dagger.hilt.android.AndroidEntryPoint
import id.rentalloka.application.R
import id.rentalloka.application.base.BaseActivity
import id.rentalloka.application.databinding.ActivityMainBinding

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    override fun setupViewBinding(): (LayoutInflater) -> ActivityMainBinding {
        return ActivityMainBinding::inflate
    }

    override fun setupViewInstance(savedInstanceState: Bundle?) {
        val navController = findNavController(R.id.main_fragment_container_view)
        binding.mainBottomNavigationView.setupWithNavController(navController)
    }
}