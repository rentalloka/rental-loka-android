package id.rentalloka.application.feature.order

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import id.rentalloka.application.R

class OrderCategoryAdapter(val fragment: Fragment): FragmentStateAdapter(fragment) {

    val ORDER_DATA_TYPE = listOf(fragment.getString(R.string.order_data_type_1), fragment.getString(R.string.order_data_type_2))

    override fun getItemCount(): Int = ORDER_DATA_TYPE.size

    override fun createFragment(position: Int): Fragment {
        TODO("Not yet implemented")
    }
}