package id.rentalloka.application.feature.onboarding.screens

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.hilt.android.AndroidEntryPoint
import id.rentalloka.application.base.BaseFragment
import id.rentalloka.application.databinding.FragmentThirdScreenBinding
import id.rentalloka.application.feature.authentication.LoginActivity

@AndroidEntryPoint
class ThirdScreen : BaseFragment<FragmentThirdScreenBinding>() {

    override fun setupViewBinding(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentThirdScreenBinding {
        return FragmentThirdScreenBinding::inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.finish.setOnClickListener {
            onBoardingFinished()
            startActivity(Intent(context, LoginActivity::class.java))
        }
    }

    private fun onBoardingFinished(){
        val sharedPref = requireActivity().getSharedPreferences("onBoarding", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putBoolean("Finished", true)
        editor.apply()
    }

}