package id.rentalloka.application.feature.order

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.rentalloka.application.R
import id.rentalloka.application.base.BaseFragment
import id.rentalloka.application.databinding.FragmentOrderContentBinding

class OrderContentFragment : BaseFragment<FragmentOrderContentBinding>() {
    override fun setupViewBinding(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentOrderContentBinding {
        return FragmentOrderContentBinding::inflate
    }


}