package id.rentalloka.application.feature.detail

import android.os.Bundle
import android.view.LayoutInflater
import androidx.viewpager2.widget.ViewPager2
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator
import id.rentalloka.application.R
import id.rentalloka.application.base.BaseActivity
import id.rentalloka.application.databinding.ActivityPropertyDetailBinding

class PropertyDetailActivity : BaseActivity<ActivityPropertyDetailBinding>(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun setupViewBinding(): (LayoutInflater) -> ActivityPropertyDetailBinding {
        return ActivityPropertyDetailBinding::inflate
    }

    override fun setupViewInstance(savedInstanceState: Bundle?) {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        /* requestWindowFeature(Window.FEATURE_NO_TITLE)
         window.setFlags(
             WindowManager.LayoutParams.FLAG_FULLSCREEN,
             WindowManager.LayoutParams.FLAG_FULLSCREEN)
         setContentView(R.layout.activity_detail_lapak)*/

        val adapter = DotIndicatorPager2Adapter()
        binding.propertyDetailImages.adapter = adapter

        val zoomOutPageTransformer = ZoomOutPageTransformer()
        binding.propertyDetailImages.setPageTransformer { page, position ->
            zoomOutPageTransformer.transformPage(page, position)
        }

        binding.propertyDetailImagesDotsIndicator.setViewPager2(binding.propertyDetailImages)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(
            MarkerOptions()
                .position(sydney)
                .title("Marker in Sydney")
        )
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }
}