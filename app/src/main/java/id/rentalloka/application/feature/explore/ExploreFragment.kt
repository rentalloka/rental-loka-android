package id.rentalloka.application.feature.explore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import id.rentalloka.application.base.BaseFragment
import id.rentalloka.application.databinding.FragmentExploreBinding
import id.rentalloka.application.feature.explore.adapter.PropertyCategoryExploreAdapter
import id.rentalloka.application.helper.PropertyType.PROPERTY_TYPE_NAME

@AndroidEntryPoint
class ExploreFragment : BaseFragment<FragmentExploreBinding>() {

    override fun setupViewBinding(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentExploreBinding {
        return FragmentExploreBinding::inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.exploreContentViewpager.apply {
            adapter = PropertyCategoryExploreAdapter(this@ExploreFragment)
            isUserInputEnabled = false
        }
        TabLayoutMediator(binding.exploreContentTitle, binding.exploreContentViewpager) { tab, position ->
            tab.text = PROPERTY_TYPE_NAME[position]
        }.attach()
    }

}