package id.rentalloka.application.feature.onboarding.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import dagger.hilt.android.AndroidEntryPoint
import id.rentalloka.application.R
import id.rentalloka.application.base.BaseFragment
import id.rentalloka.application.databinding.FragmentSecondScreenBinding

@AndroidEntryPoint
class SecondScreen : BaseFragment<FragmentSecondScreenBinding>() {

    override fun setupViewBinding(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentSecondScreenBinding {
        return FragmentSecondScreenBinding::inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val viewPager =  activity?.findViewById<ViewPager2>(R.id.viewPager)

        binding.next2.setOnClickListener {
            viewPager?.currentItem = 2
        }
    }

}