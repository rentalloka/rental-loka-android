package id.rentalloka.application.feature.post

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.rentalloka.application.R

class PostActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)
    }
}