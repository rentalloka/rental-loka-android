package id.rentalloka.application.feature.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import dagger.hilt.android.AndroidEntryPoint
import id.rentalloka.application.R
import id.rentalloka.application.base.BaseActivity
import id.rentalloka.application.databinding.ActivityLoginBinding
import id.rentalloka.application.feature.MainActivity

@AndroidEntryPoint
class LoginActivity : BaseActivity<ActivityLoginBinding>() {

    override fun setupViewBinding(): (LayoutInflater) -> ActivityLoginBinding {
        return ActivityLoginBinding::inflate
    }

    override fun setupViewInstance(savedInstanceState: Bundle?) {
        binding.toRegisterBtn.setOnClickListener {
            startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
        }
        binding.loginBtn.setOnClickListener{
            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
            finish()
        }
    }
}