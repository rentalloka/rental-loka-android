package id.rentalloka.application.feature.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import dagger.hilt.android.AndroidEntryPoint
import id.rentalloka.application.R
import id.rentalloka.application.base.BaseActivity
import id.rentalloka.application.databinding.ActivityRegisterBinding

@AndroidEntryPoint
class RegisterActivity : BaseActivity<ActivityRegisterBinding>() {

    override fun setupViewBinding(): (LayoutInflater) -> ActivityRegisterBinding {
        return ActivityRegisterBinding::inflate
    }

    override fun setupViewInstance(savedInstanceState: Bundle?) {

    }

}