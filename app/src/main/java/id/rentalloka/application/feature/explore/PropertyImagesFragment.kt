package id.rentalloka.application.feature.explore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import id.rentalloka.application.base.BaseFragment
import id.rentalloka.application.databinding.FragmentPropertyImagesBinding

@AndroidEntryPoint
class PropertyImagesFragment(private val images: String) : BaseFragment<FragmentPropertyImagesBinding>() {

    override fun setupViewBinding(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentPropertyImagesBinding {
        return FragmentPropertyImagesBinding::inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.propertyImagesImage.clipToOutline = true
        Glide.with(requireContext()).load(images).into(binding.propertyImagesImage)
    }

}