package id.rentalloka.application.feature.profile

import android.os.Bundle
import android.view.LayoutInflater
import dagger.hilt.android.AndroidEntryPoint
import id.rentalloka.application.base.BaseActivity
import id.rentalloka.application.databinding.ActivityEditProfileBinding

@AndroidEntryPoint
class EditProfileActivity : BaseActivity<ActivityEditProfileBinding>() {

    override fun setupViewBinding(): (LayoutInflater) -> ActivityEditProfileBinding {
        return ActivityEditProfileBinding::inflate
    }

    override fun setupViewInstance(savedInstanceState: Bundle?) {
        binding.editProfileBack.setOnClickListener {
            onBackPressed()
        }
    }

}