package id.rentalloka.application.feature.explore.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import id.rentalloka.application.feature.explore.PropertyImagesFragment

class PropertyImagesAdapter(fragment: Fragment, private val images: List<String>): FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = images.size

    override fun createFragment(position: Int): Fragment {
        return PropertyImagesFragment(images[position])
    }
}