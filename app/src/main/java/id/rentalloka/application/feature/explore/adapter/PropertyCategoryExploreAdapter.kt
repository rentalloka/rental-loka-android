package id.rentalloka.application.feature.explore.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import id.rentalloka.application.feature.explore.ExplorePropertyListFragment
import id.rentalloka.application.feature.explore.ExplorePropertyListFragment.Companion.CATEGORY_POSITION
import id.rentalloka.application.helper.PropertyType.PROPERTY_TYPE_NAME

class PropertyCategoryExploreAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = PROPERTY_TYPE_NAME.size

    override fun createFragment(position: Int): Fragment {
        return ExplorePropertyListFragment().apply {
            arguments = Bundle().apply {
                putInt(CATEGORY_POSITION, position)
            }
        }
    }
}