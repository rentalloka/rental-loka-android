package id.rentalloka.application.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class BaseLocalDataViewModel<T>: ViewModel() {

    protected val _resultQuery: MutableLiveData<T> = MutableLiveData()
    val resultQuery: LiveData<T> = _resultQuery

    protected val _resultCommand: MutableLiveData<T> = MutableLiveData()
    val resultCommand: LiveData<T> = _resultCommand

    protected val _disposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        _disposable.clear()
    }

}