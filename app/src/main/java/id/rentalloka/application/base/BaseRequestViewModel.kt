package id.rentalloka.application.base

interface BaseRequestViewModel<P> {

    fun doNetworkRequest(requestParams: P)

}