package id.rentalloka.application.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class BaseNetworkRequestViewModel<R, P>: ViewModel() {

    protected val _result: MutableLiveData<R> = MutableLiveData()
    val result: LiveData<R> = _result

    protected val _disposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        _disposable.clear()
    }

}