package id.rentalloka.application.base

import androidx.paging.PagingData
import io.reactivex.rxjava3.core.Flowable

interface BasePagingRequestViewModel<P, T: Any> {

    fun doNetworkRequest(requestParams: P): Flowable<PagingData<T>>

}