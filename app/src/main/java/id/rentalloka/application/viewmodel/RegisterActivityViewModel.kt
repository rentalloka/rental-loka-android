package id.rentalloka.application.viewmodel

import dagger.hilt.android.lifecycle.HiltViewModel
import id.rentalloka.application.base.BaseNetworkRequestViewModel
import id.rentalloka.application.base.BaseRequestViewModel
import id.rentalloka.application.model.AuthenticationParams
import id.rentalloka.application.model.NetworkResponse
import id.rentalloka.application.model.RegisterResponse
import id.rentalloka.application.repository.network.NetworkRepositoryImpl
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class RegisterActivityViewModel @Inject constructor(
    private val networkRepository: NetworkRepositoryImpl
): BaseNetworkRequestViewModel<NetworkResponse<RegisterResponse>, AuthenticationParams>(),
    BaseRequestViewModel<AuthenticationParams> {

    override fun doNetworkRequest(requestParams: AuthenticationParams) {
        networkRepository.register(requestParams.name.toString(), requestParams.email.toString(), requestParams.phone.toString(), requestParams.password.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { _result.postValue(NetworkResponse.Error(it.message)) }
            .doOnSubscribe {
                _result.postValue(NetworkResponse.Load(true))
                _disposable.add(it)
            }
            .subscribe({
                val accessToken = it.data?.access_token
                val registerResponse = RegisterResponse(access_token = accessToken.toString())
                _result.postValue(NetworkResponse.Success(registerResponse))
            }, {
                _result.postValue(NetworkResponse.Error(it.message))
            })
    }

}