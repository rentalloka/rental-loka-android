package id.rentalloka.application.viewmodel

import dagger.hilt.android.lifecycle.HiltViewModel
import id.rentalloka.application.base.BaseNetworkRequestViewModel
import id.rentalloka.application.base.BaseRequestViewModel
import id.rentalloka.application.model.AuthenticationParams
import id.rentalloka.application.model.NetworkResponse
import id.rentalloka.application.repository.network.NetworkRepositoryImpl
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class LogoutViewModel @Inject constructor(
    private val networkRepository: NetworkRepositoryImpl
): BaseNetworkRequestViewModel<NetworkResponse<Boolean>, AuthenticationParams>(),
    BaseRequestViewModel<AuthenticationParams> {

    override fun doNetworkRequest(requestParams: AuthenticationParams) {
        networkRepository.logout(requestParams.token.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { _result.postValue(NetworkResponse.Error(it.message)) }
            .doOnSubscribe {
                _result.postValue(NetworkResponse.Load(true))
                _disposable.add(it)
            }
            .subscribe({
                val success = it.code == 200
                _result.postValue(NetworkResponse.Success(success))
            }, {
                _result.postValue(NetworkResponse.Error(it.message))
            })
    }

}