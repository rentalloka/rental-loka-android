package id.rentalloka.application.viewmodel

import dagger.hilt.android.lifecycle.HiltViewModel
import id.rentalloka.application.base.BaseNetworkRequestViewModel
import id.rentalloka.application.base.BaseRequestViewModel
import id.rentalloka.application.model.AuthenticationParams
import id.rentalloka.application.model.LoginResponse
import id.rentalloka.application.model.NetworkResponse
import id.rentalloka.application.repository.network.NetworkRepositoryImpl
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class LoginActivityViewModel @Inject constructor(
    private val networkRepository: NetworkRepositoryImpl
): BaseNetworkRequestViewModel<NetworkResponse<LoginResponse>, AuthenticationParams>(),
    BaseRequestViewModel<AuthenticationParams> {

    override fun doNetworkRequest(requestParams: AuthenticationParams) {
        networkRepository.login(requestParams.email.toString(), requestParams.password.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { _result.postValue(NetworkResponse.Error(it.message)) }
            .doOnSubscribe {
                _result.postValue(NetworkResponse.Load(true))
                _disposable.add(it)
            }
            .subscribe({
                val accessToken = it.data?.access_token
                val loginResponse = LoginResponse(access_token = accessToken.toString())
                _result.postValue(NetworkResponse.Success(loginResponse))
            }, {
                _result.postValue(NetworkResponse.Error(it.message))
            })
    }

}