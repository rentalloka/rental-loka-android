package id.rentalloka.application.config

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import id.rentalloka.application.R
import id.rentalloka.application.feature.MainActivity
import id.rentalloka.application.feature.profile.EditProfileActivity

class ProfileFragmentConfig {

    companion object {

        @SuppressLint("UseCompatLoadingForDrawables")
        fun getConfigurations(context: Context): List<Action> {
            val configs: MutableList<Action> = mutableListOf()

            val editProfile = Action(
                icon = context.getDrawable(R.drawable.ic_baseline_edit_24),
                title = context.getString(R.string.profile_action_profile_title),
                subtitle = context.getString(R.string.profile_action_profile_subtitle),
                actionTo = Intent(context, EditProfileActivity::class.java)
            )
            configs.add(editProfile)

            val favorite = Action(
                icon = context.getDrawable(R.drawable.ic_baseline_favorite_24),
                title = context.getString(R.string.profile_action_favorite_title),
                subtitle = context.getString(R.string.profile_action_favorite_subtitle),
                actionTo = null
            )
            configs.add(favorite)

            val history = Action(
                icon = context.getDrawable(R.drawable.ic_baseline_history_24),
                title = context.getString(R.string.profile_action_history_title),
                subtitle = context.getString(R.string.profile_action_history_subtitle),
                actionTo = null
            )
            configs.add(history)

            val setting = Action(
                icon = context.getDrawable(R.drawable.ic_baseline_settings_24),
                title = context.getString(R.string.profile_action_setting_title),
                subtitle = context.getString(R.string.profile_action_setting_subtitle),
                actionTo = null
            )
            configs.add(setting)

            val support = Action(
                icon = context.getDrawable(R.drawable.ic_baseline_contact_support_24),
                title = context.getString(R.string.profile_action_support_title),
                subtitle = context.getString(R.string.profile_action_support_subtitle),
                actionTo = null
            )
            configs.add(support)

            val invite = Action(
                icon = context.getDrawable(R.drawable.ic_baseline_supervisor_account_24),
                title = context.getString(R.string.profile_action_invite_title),
                subtitle = context.getString(R.string.profile_action_invite_subtitle),
                actionTo = null
            )
            configs.add(invite)

            return configs
        }

    }

}

data class Action(
    val icon: Drawable? = null,
    val title: String? = null,
    val subtitle: String? = null,
    val actionTo: Intent? = null
)