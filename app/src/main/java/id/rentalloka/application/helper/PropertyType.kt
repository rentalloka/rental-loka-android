package id.rentalloka.application.helper

object PropertyType {

    const val STALL = 1
    const val HOME_RENT = 2
    const val HOUSE = 3
    const val APARTMENT = 4

    val PROPERTY_TYPE_NAME = listOf("Lapak", "Kontrakan", "Rumah", "Apartment")

}